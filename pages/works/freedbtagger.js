import {
  Container,
  Badge,
  Link,
  List,
  ListItem,
  UnorderedList,
  Heading,
  Center,
  Image
} from '@chakra-ui/react'
import Layout from '../../components/layouts/article'
import { ExternalLinkIcon } from '@chakra-ui/icons'
import { Title, WorkImage, Meta } from '../../components/work'
import P from '../../components/paragraph'

const Work = () => (
  <Layout title="freeDBTagger">
    <Container>
      <Title>
        freeDBTagger <Badge>2004-2009</Badge>
      </Title>
      <Center my={6}>
        <Image src="/images/works/freedbtagger_icon.gif" alt="icon" />
      </Center>
      <P>Automatic audio file tagging tool using FreeDB for Windows</P>
      
      <List ml={4} my={4}>
        <ListItem>
          <Meta>Platform</Meta>
          <span>Windows 2000/XP</span>
        </ListItem>
        <ListItem>
          <Meta>Stack</Meta>
          <span>Delphi</span>
        </ListItem>
        <ListItem>
          <Meta>Download</Meta>
          <Link href="http://odoruinu.net.s3.amazonaws.com/software/freedbtagger/fdbt105.zip">
            v1.0.5
          </Link>
        </ListItem>
        <ListItem>
          <Meta>Last update</Meta>
          <span>2009/02/16</span>
        </ListItem>
      </List>

      <Heading as="h4" fontSize={16} my={6}>
        <Center>Media coverage</Center>
      </Heading>

      <UnorderedList my={4}>
        <ListItem>
          <Link href="https://forest.watch.impress.co.jp/article/2005/02/01/freedbtagger.html">
            <Badge mr={2}>freeDBTagger</Badge>
            「freeDBTagger」{' '}
            <ExternalLinkIcon mx="2px" />
          </Link>
        </ListItem>
      </UnorderedList>

      <Heading as="h4" fontSize={16} my={6}>
        <Center>
          User reviews&nbsp;
          <Link
            target="_blank"
            href="http://www.vector.co.jp/soft/cmt/win95/art/se350576.html"
          >
            from Vector
          </Link>
        </Center>
      </Heading>

      <UnorderedList my={4}>
        <ListItem>
          
          <span>
       
          </span>
        </ListItem>
        <ListItem>
          
          <span>
 
          </span>
        </ListItem>
        <ListItem>
          
   
        </ListItem>
        <ListItem>
       

        </ListItem>
        <ListItem>
     

        </ListItem>
      </UnorderedList>

      <WorkImage src="/images/works/freedbtagger_01.jpg" alt="freeDBTagger" />
    </Container>
  </Layout>
)

export default Work
export { getServerSideProps } from '../../components/chakra'
